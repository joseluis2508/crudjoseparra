<?php

namespace App\Http\Controllers;

use App\Models\Empleados;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['enpleados']=Empleados::paginate(5);
        return view('empleado.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('empleado.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campos=[
            'Nombre'=>'required|string|max:100',
            'PrimerApellido'=>'required|string|max:100',
            'SegundoApellido'=>'required|string|max:100',
            'correo'=>'required|email',
            'foto'=>'required|max:10000|mimes:jpeg,png,jpg',
        ];
        $mensaje=[
            'required'=>'el :attribute es requerido',
            'foto.required'=>'la foto requerida',
        ];

        $this->validate($request,$campos,$mensaje);


        //$datosEmpleado = request()->all();

        $datosEmpleado = request()->except('_token');

        if($request->hasFile("foto")){
            $datosEmpleado["foto"]=$request->file("foto")->store("uploads","public");
            }

        Empleados::insert($datosEmpleado);

        //  return response()->json($datosEmpleado);
        return redirect('empleado')->with('mensaje','empleaso agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function show(Empleados $empleados)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $empleados=Empleados::findOrFail($id);
        return view('empleado.edit', compact("empleados"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        //
        $datosEmpleado = request()->except(['_token','_method']);
        
        if($request->hasFile("foto")){
            $empleados=Empleados::findOrFail($id);
            Storage::delete('public/'.$empleados->foto);
            $datosEmpleado["foto"]=$request->file("foto")->store("uploads","public");
            }
        
        Empleados::where('id','=',$id)->update($datosEmpleado);
        
        $empleados=Empleados::findOrFail($id);
        return view('empleado.edit', compact("empleados"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $empleados=Empleados::findOrFail($id);

        if(Storage::delete('public/'.$empleados->foto)){
            Empleados::destroy($id);
        }

        
        return redirect('empleado')->with('mensaje','empleado borrado');
    }
}
