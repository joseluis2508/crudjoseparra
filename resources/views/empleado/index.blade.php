@extends('layouts.app')
@section('content')
<div class="container">

@if(Session::has('mensaje'))
{{Session::get('mensaje')}}
@endif

<a href="{{url('empleado/create')}}" class= "btn btn-success"> registrar nuevo empleado </a>
<br/>
<br/>
<table class="table table-light">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>foto</th>
            <th>nombre</th>
            <th>primer apellido</th>
            <th>segundo apellido</th>
            <th>correo</th>
            <th>acciones</th>

        </tr>
    </thead>

    <tbody>
        @foreach( $enpleados as $empleados)
        <tr>
            <td>{{ $empleados->id }}</td>

            <td>
            <img class="img-thumbnail img-fluid" src="{{asset('storage').'/'.$empleados->foto }}" alt="">
            </td>
            <!-- <td>
            
            {{ $empleados->foto }}
            </td> -->

            <td>{{ $empleados->Nombre }}</td><
            <td>{{ $empleados->PrimerApellido }}</td>
            <td>{{ $empleados->SegundoApellido }}</td>
            <td>{{ $empleados->correo }}</td>
            <td>
                
            <a href="{{ url('/empleado/'.$empleados->id.'/edit') }}" class="btn btn-warning">
                Editar

            </a> 
            |
            <form action="{{ url('/empleado/'.$empleados->id ) }}" class="d-inline" method="post">
            @csrf 
            {{ method_field('DELETE') }}
            <input class="btn btn-danger" type="submit" onclick="return confirm('¿quieres borrar?')"
             value="Borrar">
            </form>

            </td>
        </tr>
        @endforeach
    </tbody>

</table>
</div>
@endsection