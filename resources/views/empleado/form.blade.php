<h1>{{ $modo }} empleado</h1>

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">
<ul>
        @foreach( $errors->all() as $error)
        <li>    {{ $error }}</li>
        @endforeach
</ul>
    </div>

    


@endif

<div class="forn-group">
<label for="nombre"> nombre</label>
<input type="text" class="form-control" name="Nombre" value="{{isset($empleados->Nombre)?$empleados->Nombre:''}}" id="Nombre">
<br>
</div>

<div class="forn-group">
<label for="PrimerApellido"> primer apellido</label>
<input type="text" class="form-control" name="PrimerApellido" value="{{ isset($empleados->PrimerApellido)?$empleados->PrimerApellido:'' }}" id="primerapellido">
<br>
</div>

<div class="forn-group">
<label for="SegundoApellido">segundo apellido</label>
<input type="text" class="form-control" name="SegundoApellido" value="{{isset($empleados->SegundoApellido)?$empleados->SegundoApellido:''}}" id="SegundoApellido">
<br>
</div>

<div class="forn-group">
<label for="correo"> correo</label>
<input type="text" class="form-control" name="correo" value="{{isset($empleados->correo)?$empleados->correo:''}}" id="correo">
<br>
</div>

<div class="forn-group">
<label for="foto"> foto</label>
@if(isset($empleados->foto))
<img src="{{asset('storage').'/'.$empleados->foto }}" width="100" alt="">
@endif
<input type="file" class="form-control" name="foto" value="" id="foto">
<br>
</div>

<label for="enviar informacion"></label>


<input  class="btn btn-success" type="submit" value="{{ $modo }} datos">

<a class="btn btn-primary" href="{{url('empleado/')}}">regresar</a>

<br>